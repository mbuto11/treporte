package main

import (
    "fmt"
    "os"
//    "os/exec"
    "log"
    "net/http"
//    "time"
//    "strings"
)


func main() {
port := os.Getenv("PORT")
if port == "" {
        port = "9000"
        log.Printf("Defaulting to port %s", port)
}

host, _ := os.Hostname()
log.Printf("%s port %s vers. %s", host, port, ver)
log.Fatal(http.ListenAndServe(fmt.Sprintf("localhost:%s", port), nil))
}
