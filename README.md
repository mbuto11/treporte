# treporte

**=Gioco delle Tre Porte=**

Un singolo giocatore gioca in 2 turni.

Al primo turno ci sono 3 porte chiuse. Dietro una di esse (a caso) è nascosto un "*premio*". Il giocatore indica una delle 3 porte.

Al secondo turno, il computer apre una delle 2 porte non indicate dal giocatore e mostra che dietro quella porta non c'è il "*premio*".
Il giocatore può indicare una delle due porte rimaste chiuse: o conferma la sua prima scelta, oppure indica l'altra porta.

Infine, il computer apre tutte le porte e mostra dove si trova il "*premio*": se il giocatore, **nel secondo turno**, ha indicato la porta dietro cui si trova il "*premio*", ha vinto. In caso contrario non ha vinto.

**Non ci sono vincite materiali**: solo la soddisfazione di avere vinto.