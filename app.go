package main

import (
    "fmt"
    "net/http"
    "regexp"
    "strings"
    "strconv"
    "time"
    "math/rand"
)

const ver = "local-2.2"

func init() {
    http.HandleFunc("/", root)
    http.HandleFunc("/help", help)
    http.HandleFunc("/ciao", ciao)
    http.HandleFunc("/finale", finale)
    http.HandleFunc("/robots.txt", robots)
    http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("images/"))))
}


func root(w http.ResponseWriter, r *http.Request) {

var oro int
oros := r.FormValue("oro")
if oros == "" {
rand.Seed(time.Now().UnixNano())
oro = rand.Intn(3)
oros = fmt.Sprintf("%d", oro)
} else {
oro1, err := strconv.Atoi(oros)
if err != nil {
fmt.Fprintf(w, "oros=%s oro1=%d err=%v", oros, oro1, err)
return
}
oro = oro1
}

rr := r.FormValue("sn")

// sanitizing input
if !checkalpha(w, rr) {
 return
}

if rr != "" {
if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}
}

alle := "ABC"
como := []string{ "chiusa", "chiusa", "chiusa" }
able := []string{ "", "", "" }
qual := "x"
gial := []string{ "box", "box", "box" }

kk := strings.Index(alle,rr)
if (kk < 0) || (kk > 2) {
 return
}

//u:=getnick(w, r, "")

if rr == "" {
  fmt.Fprintf(w,mioForm0, ver)
} else {
  gial[kk] = "evi"
  for k := 0; k < 3; k++ {
    if k != oro && k != kk {
	como[k] = "aperta"
        able[k] = "disabled"
        qual = alle[k:k+1]
	gial[k] = "non"
        break
    } else {
        como[k] = "chiusa"
        able[k] = ""
    }
  }
  fmt.Fprintf(w,mioForm1, ver, rr, qual, 
	able[0], gial[0], como[0],
	able[1], gial[1], como[1],
	able[2], gial[2], como[2],
	oros)
}


}

func finale(w http.ResponseWriter, r *http.Request) {

if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}

alle := "ABC"
gial := []string{ "box", "box", "box" }
oros := r.FormValue("oro")
oro, err := strconv.Atoi(oros)

if err != nil {
return
}

como := []string{ "aperta", "aperta", "aperta" }
rr := r.FormValue("sn")

kk := strings.Index(alle,rr)
if (kk < 0) || (kk > 2) {
 return
}
msg := ""

// sanitizing input
if !checkalpha(w, rr) {
 return
}

como[oro] = "vinc"
gial[kk] = "evi"
gial[oro] = "si"
if kk == oro {
  msg = "<b>hai vinto!!!</b>"
} else {
  msg = "non hai vinto"
}

fmt.Fprintf(w,mioForm2, ver, rr, 
	gial[0], como[0],
	gial[1], como[1],
	gial[2], como[2], msg)
}


func ciao(w http.ResponseWriter, r *http.Request) {
fmt.Fprintf(w, formCiao, ver)
}

func robots(w http.ResponseWriter, r *http.Request) {
fmt.Fprintf(w, "User-agent: *\nDisallow: /\n")
}

func checkalpha(w http.ResponseWriter, x string) bool {
if m, _ := regexp.MatchString("^[A-Za-z0-9]*$", x); !m {
    fmt.Fprintf(w, "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">")
    fmt.Fprintf(w, "<style>.red {background-color: red}</style></head>")
    fmt.Fprintf(w, "<body><span class=red>ERRORE</span>:<b>Parametro %x non valido</b><p>",x)
    fmt.Fprintf(w, "<a href='javascript:history.back()'>correggi l'errore</a><p>")
    fmt.Fprintf(w, "<a href=/>Ricomincia da capo</a>")
    fmt.Fprintf(w, "</body></html>")
    return false
}
return true
}

func help(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, formHelp, ver)
}

const mioForm0 = `
<html>
  <head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Le Tre Porte</title>
   <link rel="shortcut icon" href=/images/app.ico>
   <style>
.box { border-radius: 15px; padding-top: 15px; padding-right: 0px; padding-left: 0px; background-color: silver; }
.evi { background-color: yellow; }
td { text-align: center; }
button { border: 0; }
   </style>
  </head>
  <body>
<h1>Le Tre Porte</h1>
vers. %s
<h3>Primo Turno</h3>
<p>Ciao!
<p>Scegli una delle tre porte: A, B o C<br>
cliccandoci sopra.
<blockquote>
<p><form method=post action=/>
<table>
<tr>
<td><button class=box type=submit name='sn' value='A'><img src=images/porta_chiusa.png><br>A</button></td>
<td><button class=box type=submit name='sn' value='B'><img src=images/porta_chiusa.png><br>B</button></td>
<td><button class=box type=submit name='sn' value='C'><img src=images/porta_chiusa.png><br>C</button></td>
</tr>
</table>
</form>
</blockquote>
<div>
<small>Serve <a href=/help>aiuto</a>?<p>
<div>
</body></html>`

const mioForm1 = `
<html>
  <head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Le Tre Porte</title>
   <link rel="shortcut icon" href=/images/app.ico>
   <style>
.box { border-radius: 15px; padding-top: 0px; padding-right: 0px; padding-left: 0px; background-color: silver; }
.txtevi { background-color: yellow; }
.evi { background-color: yellow; border-radius: 15px; padding-top: 0px; padding-right: 0px; padding-left: 0px; }
.txtnon { background-color: olive; color: white; }
.non { background-color: olive; color: white; border-radius: 15px; padding-top: 0px; padding-right: 0px; padding-left: 0px; }
td { text-align: center; }
button { border: 0; }
   </style>
  </head>
  <body>
<h1>Le Tre Porte</h1>
vers. %s
<h3>Secondo Turno</h3>
<p>Ciao!
<p>Al primo turno <span class=txtevi>hai scelto la porta %s</span>.<p>
Il computer ti fa vedere che <span class=txtnon>dietro la porta %s non c'era niente</span>.<p>
Ora puoi confermare la tua scelta oppure cambiare.
<blockquote>
<p><form method=post action=/finale>
<table>
<tr>
<td><button type=submit name='sn' value='A' %s class=%s>&nbsp;<br><img src=images/porta_%s.png><br>A</button></td>
<td><button type=submit name='sn' value='B' %s class=%s>&nbsp;<br><img src=images/porta_%s.png><br>B</button></td>
<td><button type=submit name='sn' value='C' %s class=%s>&nbsp;<br><img src=images/porta_%s.png><br>C</button></td>
</tr>
</table>
<input type=hidden name=oro value=%s>
</form>
</blockquote>
<div>
<small>Serve <a href=/help>aiuto</a>?<p>
<div>
</body></html>`

const mioForm2 = `
<html>
  <head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Le Tre Porte</title>
   <link rel="shortcut icon" href=/images/app.ico>
   <style>
.box { border-radius: 15px; padding-top: 15px; padding-right: 0px; padding-left: 0px; background-color: silver; }
.txtevi { background-color: yellow; }
.evi { background-color: yellow; border-radius: 15px; padding-top: 15px; padding-right: 0px; padding-left: 0px; }
.txtsi { background-color: purple; color: white; }
.si { background-color: purple; color: white; border-radius: 15px; padding-top: 15px; padding-right: 0px; padding-left: 0px; }
td { text-align: center; }
   </style>
  </head>
  <body>
<h1>Le Tre Porte</h1>
vers. %s
<h3>Risultato Finale</h3>
<p>Ciao!
<p>Al secondo turno <span class=txtevi>hai scelto la porta %s</span>.<p>
Il computer ti fa vedere <span class=txtsi>dove si trova il "premio"</span>.<p>
<blockquote>
<p>
<table>
<tr>
<td class=%s><img src=images/porta_%s.png><br>A</td>
<td class=%s><img src=images/porta_%s.png><br>B</td>
<td class=%s><img src=images/porta_%s.png><br>C</td>
</tr>
</table>
<p>%s
<p><a href=/>Gioca ancora!</a>
</blockquote>
<div>
<small>Serve <a href=/help>aiuto</a>?<p>
<div>`

const formHelp = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Le Tre Porte</title>
<link rel="shortcut icon" href=/images/app.ico>
</head><body>
<h1>Le Tre Porte</h1>
vers. %s
<img src=/images/porta_aperta.png>
<img src=/images/porta_aperta.png>
<img src=/images/porta_vinc.png>
<p><b>Le Tre Porte</b> &egrave; un passatempo. Non si vince niente!
<p>Ci sono tre porte chiuse.<br>
Dietro una di queste porte il computer mette un "premio", rappresentato dall'immagine di alcune monete d'oro.<br>
In due turni devi indovinare dietro quale porta si trova il "premio".
<p>Al primo turno puoi indicare una delle tre porte.<br>
Al secondo turno il computer apre una delle due porte 
che <b>non</b> hai indicato
e ti fa vedere che l&igrave; <b>non</b> c'&egrave; il "premio".<br>
A quel punto puoi scegliere se <b>confermare</b> la scelta che avevi fatto 
al primo turno oppure cambiare porta, 
indicando l'altra fra le due che sono rimaste chiuse.
<p>Ora il computer apre le due porte che finora erano rimaste chiuse 
e mostra dove si trova il "premio":<br>
<blockquote><img src=images/porta_vinc.png></blockquote>
se hai indovinato, hai vinto!
<p><b>ATTENZIONE! Il gioco non prevede nessuna vincita materiale</b> (n&eacute; denaro n&eacute; altro), solo la soddisfazione di aver vinto.
<p><a href=/>Inizia a giocare!</a>
<h2>Privacy e Cookie Policy</h2>
Questo sito non utilizza nessun "cookie"<br>
</body></html>`


const formCiao = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Le Tre Porte</title>
<link rel="shortcut icon" href=/images/app.ico>
</head><body>
<img src=/images/par.jpg>
<h3>Grazie per aver giocato a Le Tre Porte.</h3>
vers. %s<p>
<a href=/>Torna presto</a> a giocare con noi.
</body></html>`
